package com.item.management.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.item.management.model.User;
import com.item.management.service.ReigsterUserService;

@RestController
public class RegisterUserController {

	@Autowired
	private ReigsterUserService reigsterUserService;

	@PostMapping("/register")
	public Boolean reigster(@RequestBody User user) {
		return reigsterUserService.reigster(user);
	}
	
	@GetMapping("/getAllUsers")
	public List<User> getAllUsers() {
		return reigsterUserService.getAllUsers();
	}
	
	
	@DeleteMapping("/deleteUsers")
	public Boolean deleteUsers() {
		return reigsterUserService.deleteUsers();
	}
	
}
