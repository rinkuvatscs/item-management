package com.item.management.repositry;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.item.management.model.User;

public interface UserRepository extends MongoRepository<User, String> {

	List<User> findByName(String name);

	User findByMobile(String mobile);
}
