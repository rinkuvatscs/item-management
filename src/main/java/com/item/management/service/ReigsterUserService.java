package com.item.management.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.item.management.model.User;
import com.item.management.repositry.UserRepository;

@Service
public class ReigsterUserService {
	
	@Autowired
	private UserRepository userRepsotry;
	
	public Boolean reigster(User user) {
		try {
			userRepsotry.save(user);
			return true;
		} catch(Exception exception) {
			return false;
		}
	}
	
	public List<User> getAllUsers() {
		return userRepsotry.findAll();
	}
	
	public boolean deleteUsers() {
		userRepsotry.deleteAll();
		return true;
	}
}
