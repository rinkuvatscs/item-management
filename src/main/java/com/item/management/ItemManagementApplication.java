package com.item.management;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories
public class ItemManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(ItemManagementApplication.class, args);
	}

}
